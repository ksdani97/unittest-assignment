package com.epam.assignment_elte.lotto;

import java.util.Arrays;
import org.junit.Test;

public class UniquenessVerifierTest {

    private UniquenessVerifier underTest = new UniquenessVerifier();
    
    @Test
    public void testVerifyWithUniqeValues() {
        // Given
        // When
        underTest.verify(Arrays.asList(1, 3, 2));
        // Then
    }
    
    @Test(expected = RuntimeException.class)
    public void testVerifyWithRepeatingValues() {
        // Given
        // When
        underTest.verify(Arrays.asList(1, 3, 2, 3));
        // Then
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testVerifyWithNull() {
        // Given
        // When
        underTest.verify(null);
        // Then
    }

}