package com.epam.assignment_elte.lotto;

import java.util.Random;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class RandomGeneratorTest {

    private RandomGenerator underTest;
    
    @Before
    public void setup() {
        Random rand = new Random(123);
        underTest = new RandomGenerator(rand);
    }
    
    @Test
    public void testGetNumberWithValidRange() {
        // Given
        // When
        int from = 1;
        int to = 9;
        int result = underTest.getNumber(from, to);
        // Then
        assertTrue(result >= from && result <= to);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testGetNumberWithInvalidRange() {
        // Given
        // When
        underTest.getNumber(5, 2);
        // Then
    }
    
    @Test
    public void testGetNumberWithMinimalRange() {
        // Given
        // When
        int number = 2;
        int result = underTest.getNumber(number, number);
        // Then
        assertEquals(result, number);
    }
}