package com.epam.assignment_elte.lotto;

import java.util.List;
import java.util.Random;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.ArgumentMatchers.any;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class LottoNumbersTest {

    private LottoNumbers underTest;
    
    private UniquenessVerifier uniquenessVerifierMock;
    
    @Before
    public void setup() {
        uniquenessVerifierMock = mock(UniquenessVerifier.class);
        underTest = new LottoNumbers(new RandomGenerator(new Random(123)), uniquenessVerifierMock);
    }
    
    @Test
    public void testGenerateNumbersWithValidRangeAndCount() {
        //Given
        doNothing().when(uniquenessVerifierMock).verify(any(List.class));
        //When
        int count = 5;
        List<Integer> result = underTest.generateNumbers(1, 90, count);
        //Then
        assertEquals(count, result.size());
        verify(uniquenessVerifierMock, times(1)).verify(any(List.class));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testGenerateNumbersWithInvalidRange() {
        //Given
        //When
        underTest.generateNumbers(50, 3, 5);
        //Then
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testGenerateNumbersWithTooSmallRange() {
        //Given
        //When
        underTest.generateNumbers(1, 3, 5);
        //Then
    }
}