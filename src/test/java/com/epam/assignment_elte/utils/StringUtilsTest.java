package com.epam.assignment_elte.utils;

import java.util.Arrays;
import java.util.Collections;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringUtilsTest {

    private StringUtils underTest = new StringUtils();
    
    @Test
    public void testJoinWithEmptyList() {
        // Given
        // When
        String result = underTest.join("abc", "xyz", Collections.emptyList());
        // Then
        assertEquals("abcemptyxyz", result);
    }
    
    @Test
    public void testJoinWithLessThanFiveElementList() {
        // Given
        // When
        String result = underTest.join("abc", "xyz", Arrays.asList(1, 3, 2, 4));
        // Then
        assertEquals("abc1324xyz", result);
    }
    
    @Test
    public void testJoinWithMoreThanFourElements() {
        // Given
        // When
        String result = underTest.join("abc", "xyz", Arrays.asList(1, 3, 2, 5, 4));
        // Then
        assertEquals("abc45231xyz", result);
    }
    
    @Test
    public void testNthElementsOnlyWithValidInput() {
        // Given
        // When
        String result = underTest.nthElementsOnly("abcdefghijklm", 4);
        // Then
        assertEquals("dhl", result);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testNthElementsOnlyWithNullInput() {
        // Given
        // When
        underTest.nthElementsOnly(null, 2);
        // Then
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testNthElementsOnlyWithNotPositiveN() {
        // Given
        // When
        underTest.nthElementsOnly("abcdefghijklm", -2);
        // Then
    }
}