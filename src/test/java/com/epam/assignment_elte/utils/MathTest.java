package com.epam.assignment_elte.utils;

import java.util.Arrays;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class MathTest {

    private Math underTest = new Math();
    
    @Test
    public void testGreatestCommonDivisorWithZeroA() {
        // Given
        // When
        int result = underTest.greatesCommonDivisor(0, 25);
        // Then
        assertEquals(25, result);
    }
    
    @Test
    public void testGreatestCommonDivisorWithZeroB() {
        // Given
        // When
        int result = underTest.greatesCommonDivisor(25, 0);
        // Then
        assertEquals(25, result);
    }
    
    @Test
    public void testGreatestCommonDivisorWithPositiveNumbers() {
        // Given
        // When
        int result = underTest.greatesCommonDivisor(100, 80);
        // Then
        assertEquals(20, result);
    }
    
    @Test
    public void testInterestRate() {
        // Given
        // When
        long result = underTest.interestRate(1000, 0.1, 5);
        // Then
        assertEquals(1610, result);
    }
    
}