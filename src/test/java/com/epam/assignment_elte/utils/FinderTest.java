package com.epam.assignment_elte.utils;

import java.util.Arrays;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FinderTest {

    private Finder underTest = new Finder();

    @Test
    public void testFindWithContainedTarget() {
        // Given
        // When
        int result = underTest.find(Arrays.asList(1, 3, 2), 3);
        // Then
        assertEquals(2, result);
    }
    
    @Test
    public void testFindWithNotContainedTarget() {
        // Given
        // When
        int result = underTest.find(Arrays.asList(1, 3, 2), 5);
        // Then
        assertEquals(-1, result);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testFindWithInvalidList() {
        // Given
        // When
        underTest.find(null, 5);
        // Then
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testFindWithInvalidTarget() {
        // Given
        // When
        underTest.find(Arrays.asList(1, 3, 2), null);
        // Then
    }
    
    @Test
    public void testFindWithNullListElement() {
        // Given
        // When
        int result = underTest.find(Arrays.asList(null, 3, 2), 3);
        // Then
        assertEquals(2, result);
    }
}