package com.epam.assignment_elte.utils;

import java.util.List;

public class StringUtils {

    /**
     * If empty list adds empty
     * If list size 4 or smaller adds elements in order
     * If higher adds them in reversed order
     */
    public String join(String start, String end, List<?> list) {
        StringBuilder builder = new StringBuilder(start);
        if (list.isEmpty()) {
            builder.append("empty");
        } else if (list.size() <= 4) { // elements should be in order if list size is 4
            for (int i = 0; i < list.size(); i++) {
                builder.append(list.get(i));
            }
        } else {
            for (int i = list.size() - 1; i >= 0; i--) { // i should be decremented and the last value of i should be 0
                builder.append(list.get(i));
            }
        }
        return builder.append(end).toString();
    }

    /**
     * creates a new string from the nth characters of the input
     */
    public String nthElementsOnly(String input, int n) {
        if (input == null) {
            throw new IllegalArgumentException("null argument"); // null input should not be allowed
        }
        if (n <= 0) {
            throw new IllegalArgumentException("n must be positive"); // only positive n should be allowed
        }
        String result = "";
        for (int i = n-1; i < input.length(); i += n) { // i should start from n-1
            result += input.charAt(i);
        }
        return result;
    }

}
